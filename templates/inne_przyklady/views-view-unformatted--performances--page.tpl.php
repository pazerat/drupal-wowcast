<div class="large-12 medium-15 column performances-list-container standalone">
  <div class="large-up-4 medium-up-2 small-up-1 row
  performances-tile-container">
    <?php foreach ($rows as $id => $row): ?>
      <div class="column">
        <?php print $row; ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
<div class="large-3 medium-15 column performances-block-container">
  <?php print views_embed_view('repertuar', 'repertoire_block'); ?>
</div>
