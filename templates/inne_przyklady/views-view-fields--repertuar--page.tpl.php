<div class="column repertoire-item-container">

    <div class="repertoire-item">

      <div class="image-container">
          <?php print $fields['field_performance_photo']->content; ?>
      </div>

      <div class="item-overlay">
        <div class="only-print">
            <div class="day"><?php print format_date($fields['field_repertoire_date']->content,
                    'day_of_month');
                ; ?><small><?php print format_date($fields['field_repertoire_date']->content,
                        'day_of_week');
                    ; ?></small></div>
            <div class="time">
                <?php print format_date($fields['field_repertoire_date']->content,
                    'hour');
                ; ?>
            </div>
            <div class="name">
                <?php print $fields['title']->content ?>
            </div>
        </div>
        <div class="item-top">
          <div class="day">
            <?php print format_date($fields['field_repertoire_date']->content,
              'day_of_month');
            ; ?>
          </div>
          <div class="date-right">
            <?php print format_date($fields['field_repertoire_date']->content,
              'day_of_week');
            ; ?>
            <br />
            <?php print format_date($fields['field_repertoire_date']->content,
              'hour');
            ; ?>
          </div>
        </div>

        <a href="<?php
        print url('node/' . $fields['nid']->content);
        ?>" class="overlay-block-link"></a>

        <div class="item-bottom">
          <a href="<?php
          print url('node/' . $fields['nid']->content);
          ?>">
            <h3><?php print $fields['title']->content ?></h3>
          </a>
          <?php if (!$fields['field_repertoire_is_info']->content) { ?>
          <a class="buy-ticket <?php if ($fields['field_reportoire_out_of_tickets']->content == "1") print 'out-of-tickets'; ?>" href="<?php print
            $fields['field_repertoire_buy_link']->content; ?>"><?php
              print ($fields['field_reportoire_out_of_tickets']->content == "1" ? 'Brak biletów' : 'Kup bilet');
              ?></a>
          <?php } else if (!empty
          ($fields['field_repertoire_info_text']->content)) { ?>
            <a href="<?php
            print url('node/' . $fields['nid']->content);
            ?>"  class="no-buy-info"><?php print
                $fields['field_repertoire_info_text']->content; ?></a>
          <?php } ?>
        </div>

      </div>

    </div>

</div>