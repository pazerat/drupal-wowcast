(function($) {

    var animate1 = true;
    var animate2 = true;

    var videoHeight = $("#headerimg video").height();
    if($(window).scrollTop() > videoHeight - 91 && animate1) {
        animate1 = false;
        animate2 = true;
        $(".navbar").addClass("notransparent");
        $(".navbar-brand img").addClass("filtered");
    }

    $(window).scroll(function () {
        videoHeight = $("#headerimg video").height();
        if($(window).scrollTop() > videoHeight - 91 && animate1) {
            animate1 = false;
            animate2 = true;
            $(".navbar").addClass("notransparent");
            $(".navbar-brand img").addClass("filtered");
        } else if ($(window).scrollTop() < videoHeight - 91 && animate2) {
            animate2 = false;
            animate1 = true;
            $(".navbar").removeClass("notransparent");
            $(".navbar-brand img").removeClass("filtered");
        }
    })
    
})(jQuery)